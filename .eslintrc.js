module.exports = {
    "extends": [
        "airbnb-base",
        "plugin:vue/recommended"
    ],
    "rules": {
        "comma-dangle": ["error", "never"],
        "array-bracket-spacing": ["error", "always"],
        "no-tabs": ["error", { allowIndentationTabs: true }],
        "indent": [
            "error",
            2
        ],
        "no-underscore-dangle": 'off',
        "vue/max-attributes-per-line": [2, {
            "singleline": 10,
            "multiline": {
                "max": 1,
                "allowFirstLine": false
            }
        }],
        "max-len": [1, {
            "code": 100,
            "comments": 120
        }]
    },
    "parserOptions": {
        "parser": "babel-eslint",
        "sourceType": "module"
    },
    "env": {
        "browser": true,
        "node": true
    }
}